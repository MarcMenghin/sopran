#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

module Sopran::Config
  require 'config/categoryData'
  
  $CONFIG_CATEGORY_SEPARATOR = "/"
  
	class ConfigManager
		attr :config
		
		def initialize
			@config = Hash.new
			self.loadDefault
		end
		
		def loadDefault
			self[Sopran::ClientBackend::CONFIG_CATEGORY_XMMS][Sopran::ClientBackend::CONFIG_XMMS_AUTOSTARTDAEMON] << true
			self[Sopran::ClientBackend::CONFIG_CATEGORY_XMMS][Sopran::ClientBackend::CONFIG_XMMS_AUTOSTARTPLAYBACK] << false
			self[Sopran::ClientBackend::CONFIG_CATEGORY_XMMS][Sopran::ClientBackend::CONFIG_XMMS_AUTOEXITONCLIENTEXIT] << false
			self[Sopran::ClientBackend::CONFIG_CATEGORY_XMMS][Sopran::ClientBackend::CONFIG_XMMS_AUTOSTOPPLAYBACK] << false
		end
		
		def [](category)
			if (not @config.has_key?(category))
				@config[category] = CategoryData.new
			end
			return @config[category]
		end
		
		def categories
			return @config.keys
		end
		
		def clear
			@config.clear
		end
	end
end