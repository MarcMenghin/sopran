#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)
require 'fileutils'
require 'optparse'

#full path of sopran.rb file
$CLIENTPATH = File.expand_path(File.dirname(__FILE__))

#add sopran.rb path to ruby search path if not already there
#this prevents errors if sopran is not started from the directory where
#sopran.rb is located.
if (not ($:.include?($CLIENTPATH) || $:.include?(File.dirname(__FILE__))))
  $: << $CLIENTPATH
end

#user home directory
# linux -> ENV HOME
# windows -> ENV HOMEDRIVE & HOMEPATH
# mac -> ??? (TODO: check on a mac)
$USERHOME = ENV["HOME"]
$USERHOME = $USERHOME != nil ? $USERHOME : (ENV["HOMEDRIVE"] + ENV["HOMEPATH"])
$USERHOME = $USERHOME != nil ? $USERHOME : "." #default fallback

#simplify Sopran debugging
$DS = $DEBUGSOPRAN ? $DEBUGSOPRAN : $DEBUG

#client requires
require 'info'
require 'lib/util'
require 'lib/debugHelper'

module Sopran
  include SopranInfo

  #constants
  CONFIG_CATEGORY_PLUGINS = "Plugins"
  #config file location
  CLIENTCONFIGDIRECTORY = File.join($USERHOME, ".config", "xmms2", "clients", CLIENTDIRECTORYNAME)
  #log location
  CLIENTLOGDIRECTORY = File.join(CLIENTCONFIGDIRECTORY, "logs")
  class SopranMain
    include Util

    @@logger = nil

    public

    attr_reader :pluginManager, :xmms2Client, :configManager
    def initialize
      @xmms2Client = nil
      @configManager = nil
      @pluginManager = nil
      @cmdOptions = nil

      puts "#{Sopran::CLIENTNAME} #{Sopran::CLIENTVERSION}"

      #parse cmd line first for speed
      loadCommandLine()

      #additional needen requires (loaded late so cmd line is handeled fast)
      require 'clientBackend'
      require 'plugins/pluginManager'
      require 'config/configManager'

      #load logging
      if (@@logger == nil)
        loadLogging()
        @@logger = Log4r::Logger.new(self.class().name)
      end

      if ($DS)
        self.printEnvData(@@logger)
        printCMDStuff()
      end
      
      #load the different parts
      loadConfiguration()
      loadXMMS2Client()
      loadPluginManager()
    end

    def run
      begin
        startup()
        @pluginManager.runMainloop()
      rescue SystemExit
        puts "Exiting #{Sopran::CLIENTNAME}."
      ensure
        shutdown()
        puts "Bye, bye ;)"
      end
    end

    private

    def startup
      @@logger.info "#{Sopran::CLIENTNAME} Startup"
      @pluginManager.startup(@xmms2Client, @pluginManager, @configManager, @cmdOptions)

      @pluginManager.printPlugins
      @@logger.info "#{Sopran::CLIENTNAME} Startup finished"
    end

    def shutdown
      @@logger.info "#{Sopran::CLIENTNAME} Shutdown"
      @pluginManager.shutdown
      @xmms2Client.printLastError

      #stop playback if set
      if (@configManager[Sopran::ClientBackend::CONFIG_CATEGORY_XMMS][Sopran::ClientBackend::CONFIG_XMMS_AUTOSTOPPLAYBACK])
        @xmms2Client.stop
      end

      #exit deamon if set
      if (@configManager[Sopran::ClientBackend::CONFIG_CATEGORY_XMMS][Sopran::ClientBackend::CONFIG_XMMS_AUTOEXITONCLIENTEXIT])
        @xmms2Client.exitXMMS2Deamon
      end

      @pluginManager.printPlugins
      @@logger.info "#{Sopran::CLIENTNAME} Shutdown finished"
    end

    def loadCommandLine
      @cmdOptions = {}
      @cmdFiles = []
      optparse = OptionParser.new do |opts|
        # Set a banner, displayed at the top
        # of the help screen.
        opts.banner = "Usage: sopran [options] file1 file2 ..."
        # Define the options, and what they do
        @cmdOptions[:profile] = nil
        opts.on( '-p', '--profile', 'Set a profile to start (default: trayicon)' ) do |profile|
          @cmdoptions[:profile] = profile
        end
        @cmdOptions[:verbose] = false
        opts.on( '-v', '--verbose', 'Output more information' ) do
          @cmdOptions[:verbose] = true
        end
        @cmdOptions[:quick] = false
        opts.on( '-d', '--debug', 'Increases log output' ) do
          @cmdOptions[:debug] = true
          $DS = true
        end

        @cmdOptions[:logfile] = nil
        opts.on( '-l', '--logfile FILE', 'Write log to FILE' ) do |file|
          @cmdOptions[:logfile] = file
        end

        opts.separator ""
        opts.separator "Common options:"

        # This displays the help screen, all programs are
        # assumed to have this option.
        opts.on_tail( '-h', '--help', 'Display this screen' ) do
          puts opts
          exit
        end

        # Another typical switch to print the version.
        opts.on_tail("--version", "Show version") do
          puts OptionParser::Version.join('.')
          exit
        end
      end

      optparse.program_name = "#{Sopran::CLIENTNAME}"
      optparse.version = "#{Sopran::CLIENTVERSION}"
      optparse.release = ""

      # Parse the command-line. Remember there are two forms
      # of the parse method. The 'parse' method simply parses
      # ARGV, while the 'parse!' method parses ARGV and removes
      # any options found there, as well as any parameters for
      # the options. What's left is the list of files to resize.
      begin
        optparse.parse!
      rescue
        puts ""
        puts "Unknown Option."
        puts ""
        puts optparse.help()
        exit
      end

      #get files from command-line
      @cmdOptions[:files] = []
      ARGV.each do |file|
        @cmdOptions[:files] << file
      end
      if not @cmdOptions[:files].empty?
        @cmdOptions[:profile] = "cmdline"
      end
    end

    def printCMDStuff
      @@logger.info "CMDLine Options:"
      @cmdOptions.each do |key, value|
        @@logger.info " » #{key} => #{value}"
      end
    end

    def loadConfiguration
      #create config location if not already there
      if (not File.exist?(CLIENTCONFIGDIRECTORY))
        @@logger.info "Config directory not found, will try to create it."
        @@logger.info "New Config directory: #{CLIENTCONFIGDIRECOTRY}"
        FileUtils.mkdir_p(CLIENTCONFIGDIRECTORY)
      end

      @configManager = Config::ConfigManager.new

      #load basic defaults
      @configManager[Sopran::ClientBackend::CONFIG_CATEGORY_XMMS][Sopran::ClientBackend::CONFIG_XMMS_AUTOSTARTDAEMON] << true
      @configManager[Sopran::ClientBackend::CONFIG_CATEGORY_XMMS][Sopran::ClientBackend::CONFIG_XMMS_AUTOSTARTPLAYBACK] << false
      @configManager[Sopran::ClientBackend::CONFIG_CATEGORY_XMMS][Sopran::ClientBackend::CONFIG_XMMS_AUTOEXITONCLIENTEXIT] << false
      @configManager[Sopran::ClientBackend::CONFIG_CATEGORY_XMMS][Sopran::ClientBackend::CONFIG_XMMS_AUTOSTOPPLAYBACK] << false
    end

    def loadXMMS2Client
      #init xmms2 client
      @xmms2Client = ClientBackend.new
      @xmms2Client.connectSave(@configManager[ClientBackend::CONFIG_CATEGORY_XMMS][ClientBackend::CONFIG_XMMS_AUTOSTARTDAEMON])
      if (@xmms2Client.connected?)
        @xmms2Client.printStats
        @xmms2Client.printPlaylists
      end
    end

    def loadPluginManager
      #load plugins
      @pluginManager = Plugins::PluginManager.new

      #add default paths
      @pluginManager << File.join($CLIENTPATH, "plugins")
      #@pluginManager << File.join($CLIENTPATH, "examplePlugins")
      @pluginManager << File.join(CLIENTCONFIGDIRECTORY, "plugins")

      #search and load plugins
      @pluginManager.loadPlugins()

      if ($DS)
        self.printRubySearchPaths(@@logger)
        @pluginManager.printPlugins()
      end
    end

    def loadLogging
      require 'log4r'
      require 'log4r/configurator'

      #create log directory
      if (not File.exist?(CLIENTLOGDIRECTORY))
        puts "Log directory not found, will try to create it."
        puts "Log directory: #{CLIENTLOGDIRECTORY}"
        FileUtils.mkdir_p(CLIENTLOGDIRECTORY)
      end

      #loading logging from xml file
      if ($DS)
        puts "Loading logging definition from:"
        puts " #{File.join($CLIENTPATH, 'logging.xml')}"
      end
      Log4r::Configurator['logpath'] = CLIENTLOGDIRECTORY + File::SEPARATOR
      Log4r::Configurator.load_xml_file(File.join($CLIENTPATH, 'logging.xml'))

      logger = Log4r::Logger['Sopran']
      if (logger == nil)
        #if baselogger is not defined in XML define it
        logger = Log4r::Logger.new 'Sopran'
      end

      if ($DS)
        logger.level = Log4r::ALL
        #logger.outputters=Log4r::Outputter.stdout()
        logger.add('consoleOut')

        logger.info "added console logger output as ruby debug flag is set"
      end

      printBasicInformationToLog()
    end

    def printBasicInformationToLog
      logger = Log4r::Logger['Sopran']
      loglevel = logger.level

      #print some basic information to the log file
      logger.level = Log4r::ALL
      logger.info "#{Sopran::CLIENTNAME} #{Sopran::CLIENTVERSION}"
      logger.info "#{$CLIENTPATH}"
      logger.info "Log: #{CLIENTLOGDIRECTORY}"
      logger.info "Config: #{CLIENTCONFIGDIRECTORY}"
      logger.info "UserHome: #{$USERHOME}"
      logger.info((Time.new).strftime("Time: %Y-%m-%d %H:%M:%S"))
      logger.level = loglevel
    end
  end
end

# Main program entry point
main = Sopran::SopranMain.new
main.run

