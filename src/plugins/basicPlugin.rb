# -*- coding: utf-8 -*-
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)
module Sopran::Plugins
  class BasicPlugin
    @@logger = nil

    attr_accessor :running, :enabled
    attr_reader :name, :version, :description, :id
    
    def initialize
      if @@logger == nil then
        @@logger = Log4r::Logger.new(self.class().name)
      end
      @name = "No Name"
      @version = "No Version"
      @description = "No Description Defined"
      @id = :pluginNone
      @running = false;
      @enabled = true;
    end

    def startup (pluginContext)
      puts "Startup of plugin '#{@name}' not implemented"
      return true
    end

    def shutdown (pluginContext)
      puts "shutdown of plugin '#{@name}' not implemented"
    end
  end

end