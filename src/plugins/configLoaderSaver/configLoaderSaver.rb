#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)
require 'rexml/document'
require 'rexml/element'

module Sopran::Plugins::ConfigLoaderSaver
	class ConfigLoaderSaver
		attr_reader :configPath
		CONFIG_DOCROOT = "sopran_config"
		CONFIG_VALUE = "value"
		CONFIG_TYPE = "type"
		CONFIG_NAME = "name"
		CONFIG_CATEGORY = "category"
		
		def initialize(configPath)
			@configPath = configPath
		end
		
		def load(config)
			puts "Loading configuration from xml file."
			begin
				file = File.new(@configPath, File::RDONLY)
				document = REXML::Document.new(file)
				self.transformFromXMLToConfig(config, document.root)
			rescue
				puts "could not load file."
			ensure
				if (file != nil)
					file.close
					file = nil
				end
			end
		end
		
		def testValues(config)
			test = "Test Cases"
			config[test]["string"] << "test string"
			config[test]["integer"] << 123456
			config[test]["bigint"] << 79229992514299337593549950336
			config[test]["boolTrue"] << true
			config[test]["boolFalse"] << false
			config[test]["float"] << 0.1245
		end
		
		def save(config)
			puts "Saving configuration to xml file."
			
			if ($DEBUG)
				#set test values (each supported type once)
				self.testValues(config)
			end
			
			begin
				#create xml document
				document = REXML::Document.new()
				root = REXML::Element.new(CONFIG_DOCROOT)
				document << root
				self.createElements(config, root)
			
				#write to file
				file = File.new(@configPath, File::CREAT|File::WRONLY|File::TRUNC)
				document.write(file, 0, true)
			rescue
				puts "ERROR: could not write XML file."
			ensure
				if (file != nil)
					file.close
					file = nil
				end
			end
		end
		
		def transformFromXMLToConfig(config, root)
			if (root.has_name?(CONFIG_DOCROOT))
				root.each_element {|element1|
					if element1.has_name?(CONFIG_CATEGORY)
						category = element1.attribute(CONFIG_NAME)
						if ($DEBUG)
							puts ">>> Category: #{category}"
						end
						
						element1.each_element {|element2|
							if (element2.has_name?(CONFIG_VALUE))
								name = element2.attribute(CONFIG_NAME)
								type = element2.attribute(CONFIG_TYPE)
								value = element2.get_text
								begin
									realValue = transformStringToValue(value, type)
									config[category][name] << realValue
								rescue
									puts "ERROR reading a config value."
								end
							end
						}
					end
				}
				if ($DEBUG)
					puts ""
				end
			end
		end
		
		def transformStringToValue(value, type)
			result = nil
			if ($DEBUG)
				puts "  >> #{value} [#{type}]"
			end
			case type
			when true.class.to_s
				result = true
			when false.class.to_s
				result = false
			when ''.class.to_s
				result = value
			when 0.class.to_s, 79228162514264337593543950336.class.to_s
				result = value.to_i
			when 0.0.class.to_s
				result = value.to_f
			when nil.class.to_s
				result = nil
			end
			return result
		end
		
		def createElements(config, root)
			config.categories.each {|key|
				category = REXML::Element.new(CONFIG_CATEGORY, root)
				category.add_attribute(CONFIG_NAME, key)
				config[key].names.each {|name|
					value = config[key][name]
					if (value.value != value.defaultValue)
						valueNode =  REXML::Element.new(CONFIG_VALUE, category)
						valueNode.add_attribute(CONFIG_NAME, name)
						valueNode.add_attribute(CONFIG_TYPE, value.value.class.to_s)
						valueNode.add_text(value.value.to_s)
					end
				}
			}
		end
	end
end