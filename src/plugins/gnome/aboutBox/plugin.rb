#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)
require 'plugins/basicPlugin'

module Sopran::Plugins::AboutBox
  class Plugin < Sopran::Plugins::BasicPlugin
    PLUGIN_PATH = File.expand_path(File.dirname(__FILE__))
    def initialize
      super()
      @name = "About Box"
      @version = "v0.2"
      @description = "The default About dialog."
      @id = :pluginGnomeAboutBox
      @builder = nil
    end

    def startup (pluginContext)
      begin
        require 'gtk2'
        return true
      rescue
        @@logger.error "Can't find binding to GTK2."
        return false
      end
    end

    def shutdown (pluginContext)
      self.hide
    end

    def show
      if (defined? 'Gtk' && @builder == nil)
        @builder = Gtk::Builder.new
        @builder.add_from_file(File.join(PLUGIN_PATH, "aboutWindows.glade"))
        @aboutWindow = @builder["aboutWindow"]
        @closeButton = @builder["aboutClose"]

        @aboutCloseSignal = @aboutWindow.signal_connect("destroy") { |w| hide }
        @closeButtonSignal = @closeButton.signal_connect("clicked") { |w|
          hide
        }

        @@logger.info "Showing #{@name}."
        @aboutWindow.show
      end
    end

    def hide
      if (@builder != nil)
        @aboutWindow.hide
        @closeButton.signal_handler_disconnect(@closeButtonSignal)
        @aboutWindow.signal_handler_disconnect(@aboutCloseSignal)
        @closeButton = nil
        @closeButtonSignal = nil
        @aboutWindow = nil
        @builder = nil
        @@logger.info "Hiding #{@name}."
      end
    end
  end
end
