#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)
require 'plugins/basicPlugin'

module Sopran::Plugins::JumpBox
	
class Plugin < Sopran::Plugins::BasicPlugin
	PLUGIN_PATH = File.expand_path(File.dirname(__FILE__))
	def initialize
		super()
		@name = "JumpBox"
		@version = "v0.1"
		@description = "A JumpBox similar to the one found in WinAMP"
		@id = :pluginJumpBox
	end
	def startup (pluginContext)
		return true
	end
	
	def shutdown (pluginContext)
	end		
end

end
