#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)
require 'plugins/basicPlugin'

module Sopran::Plugins::MultimediaKeys
	
class Plugin < Sopran::Plugins::BasicPlugin
	PLUGIN_PATH = File.expand_path(File.dirname(__FILE__))
	def initialize
		super()
		@name = "MultimediaKeys"
		@version = "v0.1"
		@description = "Grabbs global multimedia keys."
		@id = :pluginMultimediaKeys
	end
	def startup (pluginContext)
		return true
	end
	
	def shutdown (pluginContext)
	end		
end

end
