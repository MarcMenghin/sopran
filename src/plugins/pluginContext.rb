# -*- coding: utf-8 -*-
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

module Sopran::Plugins
  class PluginContext
    @@logger = nil

    public
    
    attr_reader :xmms2Client, :pluginManager, :configManager, :mainObject, :cmdOptions
    def initialize(xmms2Client, pluginManager, configManager, cmdOptions)
      if @@logger == nil then
        @@logger = Log4r::Logger.new(self.class().name)
      end

      @xmms2Client = xmms2Client
      @pluginManager = pluginManager
      @configManager = configManager
      @cmdOptions = cmdOptions
      @mainObject = nil
    end

    def existsPlugin?(id)
      return @pluginManager.exists?(id)
    end

    def reloadAllPlugins
      puts "--> Try to reload plugins <--"
      @pluginManager.shutdown
      @pluginManager.loadPluginDirectories
      @pluginManager.loadPlugins
      @pluginManager.startup
    end

    def exitPlatform
      if @mainObject != nil
        @mainObject.exitProgramm()
      else
        #error no main exiting anyway
        @@logger.info "Exit #{Sopran::CLIENTNAME} requested but no mainObject registered. Exiting from context."
        exit()
      end
    end

    def registerMain(mainObj)
      if @mainObject != nil
        @@logger.warn "Already a mainObject registered."
        @@logger.warn "  first: #{@mainObject.class().name}"
        @@logger.warn "  second: #{mainObj.class().name}"
      end
      @mainObject = mainObj
    end

  end
end