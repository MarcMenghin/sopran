= Sopran - A XMMS2 Client written in ruby =
================================== v0.2 ===

The Name:
----------
  'Sopran' is the german word for soprano, which is a high voice type.
  Wikipedia:
    < http://de.wikipedia.org/wiki/Sopran > (german)
    < http://en.wikipedia.org/wiki/Soprano > (english)

Requirements:
--------------
	- GTK+
	- Gnome2	
	- ruby 1.8+
	- log4r
	- XMMS2 0.7 DrNo + ruby bindings
	
Users:
-------
  It is only tested on Ubuntu 10.04. As I currently have not the resources to
  test it on other operating systems, let me know if it works on a
  different platform for you.
  
  Requirements:
    - ruby 1.8+
    - libruby-extras 0.5+
    - ruby-gnome2 0.19+
    - log4r 1.0+
    - XMMS2 version 0.7+ DrNo with ruby bindings (Download this from the
        XMMS2 homepage. You have to compile it for yourself.)
  
  Installation:
    Copy all files where you want them to be.
    
  Start Sopran:
    Start the client by calling './sopran.rb' from where you have installed it.
    You should see a tray icon appear.
   
Developers:
------------
  For information about development take a look at the 'developer.txt' file.
  
License:
---------
  for the license take a look at the provided 'license.txt' file.
  
Feedback, Bugs, etc:
---------------------
  In the current stage of development feedback from end-users isn't needed.
  There are a lot of new things planned and partly already implemented. If you
  want something really special, never seen in an other audio player before,
  feel free to contact me. As the player gets more stable and mature, feedback
  is very welcome. Best way to contact me is over the digitalDreams boards
  located at: < http://forum.digital-d.at/ >
  
Contributors:
--------------
  Marc Menghin <marc.m@digital-d.at> (original creator)
  
Thanks to:
-----------
  The people of the XMMS2 Community which helped me a lot with my questions.
  My flatmate which I border with stupid questions all the time.
  Also thanks to Simon for bringing me to ruby and for helpful discussions.
  