= Sopran - A XMMS2 Client written in ruby =
======================= developer guide ===

Introduction:
--------------
  I have started to work on Sopran to teach myself ruby, which I think is a
  very nice language. Furthermore I always wanted to create a client for XMMS2
  because I love the architecture (server-client, collections,..). I am also
  new to Gnome (Gtk+) development which is interesting too. This document
  should give you a better understanding of the basic architecture to encourage
  a hopefully healthy discussion about it. So I really like to hear about your
  suggestions for improvements, especially on the platform part.
  
The Platform:
--------------
  The basic idea is a platform where everyone is able to create plugins and
  just use the provided functionality. With this in mind the basic platform in
  its current state provides the following things:
    - Plugin Framework
    - Configuration Manager (currently to store string-string values)
    - XMMS2 Client Backend (to communicate with xmms2d)
  I tried to put as much as possible into plugins, so the framework is very
  lightweight and thin. With that most parts of the client are very easy to
  replace.
  
  Additionally the platform itself controls xmms2d. If the platform starts
  without any running xmms2d it will start the daemon by calling
  'xmms2-launcher'. It will also call the quit on the xmmsclientlib before the
  platform exits if the option is set. Generally it respects the guidelines set
  by the people of xmms2. It is also able to just stop playback if the platform
  exits.
  
  The Platform is as platform independent as possible in the current stage.
  There are only very few dependencies to GTK and Gnome from the platform (one
  for each as far as I remember, all in 'sopran.rb'). 
  
Plugin Framework:
------------------
  Paths are added to the PluginManager. Which searches this paths
  sub-directories for a 'plugin.rb' file and loads it. By default it searches
  the '<install_dir>/plugins' path and the user directory at
  '<user_home>/.config/xmms2/sopran/plugins'.
  
  from the plugin class located in the 'plugin.rb' a object is created and the
  method startup is called which should activate the plugin. Before a normal
  program end the method shutdown is called so the plugin can unload correctly.
  
  A plugin has access to almost anything, even other plugins. All is done over
  the PluginContext object. PluginManager, ConfigManager and XMMS2Client all
  are available in the context object. The context also has a few methods to
  communicate with the platform.

Configuration Manager:
-----------------------
  The configuration manager is very simple at the moment. It only stores a
  string to object mapping of values. This is likely to completely change in
  the future.
  
  There is already a plugin which writes the configuration to a config file.
  This plugin supports saving and loading of string to Boolean, String,
  Integer and Float values. It stores everything in an XML format.
  
XMMS2 Client Backend:
----------------------
  This is a simple layer over the libxmmsclient. It provides some higher level
  functionality. And should later on used to abstract sync and async calls to
  xmms2d. Hopefully this will simplify the usage for other people. It will
  always provide the connected libxmmsclient for advanced usages.
  
A Sopran Plugin:
-----------------
  A plugin can hook itself into the system by the provided PluginContext.
  Within this context all important objects are located. The ConifgManager,
  PluginManager, XMMS2ClientBackend, aso. So every plugin has basically
  access to everything, which means freedom, I hope.
  
  A plugin provides functionality to other plugins by events and methods it 
  defines in the 'plugin.rb' file. This events and methods are for public use. 
  
Future Plans for v0.2:
-----------------------
  - Improve plugin system
      - load, unload of plugins and paths
      - better plugin interaction (events for newly loaded plugins)
  - Improve config system
      - add Array and Hash support to 'configLoaderSaver' plugin
      - finish implementation of 'configUI' plugin
  - Create a better way for plugins to provide services to other plugins and
    the platform itself.
  - If I have time:
      - finish OSD plugin
      - finish multimediaKeys plugin

Feedback, Bugs:
----------------
  I like to hear about your thoughts on the 'big picture'. I am still working
  on the architecture, so feedback there is very welcome. This also means that
  the current API is very unstable and likely to change. If you have any
  changed in mind that you think would be of benefit to the platform let me
  know. Best way to contact me right now is over mail < marc.m@digital-d.at > 
  or the digitalDreams forum at: < http://forum.digital-d.at/ >

Get involved:
--------------
  If you want to contribute anything contact me (main, board). Currently I work
  over a local SVN repository (possible access over hamachi) using Eclipse and
  the DLT. This will change sooner or later anyway so I am oper for almost
  everything ;). I would be most interested in people willing to do some work
  in the following areas:
    - update system (similar to the one eclipse uses just not so complex)
    - plugins for other platforms (mac, windows, kde, ...)
    - help on GTK plugins
  But I am open to any other help as well.
   