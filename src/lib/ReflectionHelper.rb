# -*- coding: utf-8 -*-
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

module Sopran::Lib
  class ReflectionHelper
    
    attr_reader :searchPattern, :classes
    
    def initialize(pattern)
      @searchPattern = pattern
      @classes = Array.new
    end
    
    private
    
    def searchForClassDefinitionsInObjectSpace
      ObjectSpace.each_object() { |obj|
        ident = obj.class.to_s
        name = obj.to_s
        
        if (ident == "Class" && (regex.match(name)) != nil && !@classes.include?(obj))
          @classes << obj
        end
      }
    end
  end

end