# -*- coding: utf-8 -*-
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

require 'lib/util'

module Sopran::Lib
	class SongInfo
		attr_reader :info
		
		def initialize(propDict = nil)
			@info = propDict
		end
		
		def title
			return @info[:title]
		end
		
		def artist
			return @info[:artist]
		end
		
		def album
			return @info[:album]
		end
		
		def comment
			return @info[:comment]
		end
		
		def track
			return @info[:tracknr]
		end
		
		def duration
			return @info[:duration]
		end
		
		def dirationFormated
			return format_time(duration)
		end
		
		def genre
			return @info[:genre]
		end
		
		def added
			return @info[:added]
		end
		
		def timesPlayed
			return @info[:timesplayed]
		end
		
		def url
			return @info[:url]
		end
		
		def lastPlayed
			return @info[:laststarted]
		end
		
		def bitrate
			return @info[:bitrate]
		end
		
		def channels
			return @info[:channels]
		end
		
		def mime
			return @info[:mime]
		end
		
		def id
			return @info[:id]
		end
		
		def samplerate
			return @info[:samplerate]
		end
		
		def format(str)
			result = str.clone
			result.gsub!(/%([A-Za-z0-9]+)%/n) {
					match = $1.dup
					case match
					when /\Atitle\z/ni          then self.title
					when /\Aartist\z/ni         then self.artist
					when /\Acomment\z/ni        then self.comment
					when /\Atrack\z/ni          then self.track
					when /\Aalbum\z/ni          then self.album
					when /\Agenre\z/ni          then self.genre
					when /\Aadded\z/ni          then self.added
					when /\Alastplayed\z/ni     then self.lastplayed
					when /\Aurl\z/ni          	then self.url
					when /\Atimesplayed\z/ni    then self.timesPlayed
					when /\Abitrate\z/ni        then self.bitrate
					when /\Achannels\z/ni       then self.channels												
					when /\Amime\z/ni           then self.mime
					when /\Asamplerate\z/ni     then self.samplerate
					when /\Aid\z/ni             then self.id
					else
						match
					end
				}			
				result
		end
		
		def printValues
			puts "==== Info ==============================================="
			@info.each { |key, value|
				x = key.class.to_s
				y = value.class.to_s
				puts "-> key: #{key} value: #{value}"
			}
			puts ""			
		end
	end
end