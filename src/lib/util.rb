# -*- coding: utf-8 -*-
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

module Util
	def format_time(time)
		minutes = (time / 1000) / 60
		seconds = ((time / 1000) - (minutes * 60)).to_s.rjust(2, '0')
		hours = (minutes / 60)
		days = hours / 24
		if (hours > 0)
			minutes = minutes - (hours * 60)
			if (days > 0)
				hours = hours - (days * 24)
			end
		end
		minutes = minutes.to_s.rjust(2, '0')
		hours = hours.to_s.rjust(2, '0')
		days = days.to_s
		if (days != '0')
			return "#{days}d #{hours}:#{minutes}:#{seconds}"
		else
			if (hours != '00')
				return "#{hours}:#{minutes}:#{seconds}"
			else
				return "#{minutes}:#{seconds}"
			end
		end
	end	
	
  def printRubySearchPaths(logger)
      logger.info "Ruby search paths: "
        $:.each { |value|
          logger.info " » #{value}"
        }
  end
  
  def printEnvData(logger)
    logger.info "ENV entries: "
    ENV.each { |key, value|
      logger.info "  #{key} » #{value}"
    }
  end
end