# -*- coding: utf-8 -*-
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

module Util::DebugHelper
  def printInfo(obj)
    puts "Type: "
    printType(obj)

    puts "Methods: "
    printMethods(obj)
    
    puts "Value: "
    if obj.kind_of?(Hash)
      printHash(obj)
    elsif obj.kind_of?(Array)
      printArray(obj)
    else
      puts "#{obj.to_s}"
    end

  end

  def printType(obj)
    puts "#{obj.class.name} (\##{obj.object_id})"
  end

  def printHash(obj)
    obj.each { |key, value|
      puts " » #{key} => #{value}"
    }
  end

  def printArray(obj)
    obj.each { |value|
      puts " » #{value}"
    }
  end

  def printMethods(obj)
    obj.public_methods.each {|value|
      puts " » #{value}"
    }
  end

  module_function :printType, :printHash, :printArray, :printMethods, :printInfo
end