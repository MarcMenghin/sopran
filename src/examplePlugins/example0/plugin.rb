#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

require 'plugins/basicPlugin'

module Sopran::Plugins::Example0
	class Plugin < Sopran::Plugins::BasicPlugin
		PLUGIN_PATH = File.expand_path(File.dirname(__FILE__))
		
		def initialize
			super()
			@name = "Example-0"
			@version = "v0.1"
			@description = "Example 0"
			@id = :pluginExample0
		end		
		
		def startup (pluginContext)
			puts "Example-0: Hello World (startup)"
			puts "Example-0 path: #{PLUGIN_PATH}"
					
			return true
		end
	
		def shutdown (pluginContext)
			puts "Example-0: Hello World (shutdown)"
		end
	end	
end
