#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

require 'plugins/basicMainPlugin'

module Sopran::Plugins::Example667
	class Plugin
		PLUGIN_PATH = File.expand_path(File.dirname(__FILE__))
		
		def initialize
			super()
			@name = "Example-667"
			@version = "v0.1"
			@description = "Example 667 (Evil2) - The I-don't-extend-the-right-class plugin."
			@id = :pluginExample667
		end		
	end	
end
