#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

require 'plugins/basicMainPlugin'

module Sopran::Plugins::Example666
	class Plugin < Sopran::Plugins::BasicMainPlugin
		PLUGIN_PATH = File.expand_path(File.dirname(__FILE__))
		
		def initialize
			super()
			@name = "Example-666"
			@version = "v0.1"
			@description = "Example 666 (Evil) - The I-don't-implement-anything plugin."
			@id = :pluginExample666
		end		
	end	
end
